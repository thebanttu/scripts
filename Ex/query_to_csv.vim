" For the whole file, do this substitute.
%substitute/--[-+]*\| \+| \+\| *[+|]$\|^[+|] */,/ge
" For the whole file, delete 2 consecutive commas

silent global/^,,/d
" Remove the first and last comma.
%substitute/^,\|,$//ge
