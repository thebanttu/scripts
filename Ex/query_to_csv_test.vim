" For the whole file, do this substitute.
%substitute/--[-+]*\| \+| \+\| *[+|]$\|^[+|] */,/g
" For the whole file, delete 2 consecutive commas
global/^,,/d
" Remove the first and last comma.
%substitute/^,\|,$//g
global/^/p
echo expand('%')
w %
quit
