#!/bin/bash
. $HOME/.scr/bash/colors.sh

function launch {
    cd ~/.scr/tmux
    bash homeground.sh
    cd -
}

function lnk {
    cd ~/.scr/tmux
    bash link_utils.sh
    cd ~
}

function play {
    echo -e "You searched for -> $1.\n"
    # Why not use a file listing so we can show the user, me :) what is playing
    find ~/Music -iname "*$1*" -type f | tee /tmp/mplay_list.txt
    mpv -playlist /tmp/mplay_list.txt
}

function msc {
    echo -e "You searched for -> $1.\n"
    find ~/Music -iname "*$1*" -type f | tee /tmp/mplay_list.txt
}

function vplay {
    IFS='|'
    echo -e "You searched for -> $*.\n\nYour search matched:"
    #find ~/Music -iname "*$1*" -type f -exec mplayer -profile pulse,novid,shhh {} +
    # Why not use a file listing so we can show the user, me :) what is playing
    find_iname_args=$(echo "$*" | perl -nle 'print substr(join(" ",map{qq/-iname \047*$_*\047 -o/}split/\|/),0,-3)')
    regex="-regextype posix-extended -regex '.*(mkv|mp4|avi|webm)$' "
    [ ! -d ~/Videos ] && mkdir -p ~/Videos
    [ ! -d ~/rDownloads ] && mkdir -p ~/rDownloads
    paths="~/Videos ~/rDownloads ~/Music"
    find_comm="find $paths $find_iname_args $regex -type f | tee /tmp/mplay_list.txt |\
        sed 's!.*/!!g'"
    if [ $# -eq 0 ]; then
        eval "$find_comm" | less
    else
        eval "$find_comm"
        mpv -playlist /tmp/mplay_list.txt
    fi
}

function fplay {
    IFS='|'
    playlist_fl='/tmp/mplay_list.txt'
    echo -e "You searched for -> $*.\n\nYour search matched:"
    find_iname_args=$(echo "$*" | perl -nle 'print substr(join(" ",map{qq/-iname \047*$_*\047 -o/}split/\|/),0,-3)')
    find_comm="find ~/Music $find_iname_args -type d | tee /tmp/fplay_list.txt |\
        sed -e 's!.*/!!g' -e 's/^/- /'"
    eval "$find_comm"
    echo
    echo "" > $playlist_fl
    regex="-regextype posix-extended -regex '.*(mkv|mp4|avi|webm|mp3|flac|wav|ogg)$' "
    while read dir; do
        echo "Playing from dir -> $dir"
        echo
        # Escape file chars
        dir=$(echo "$dir" | sed "s/.*/'&'/")
        find_comm="find $dir $regex -type f | tee -a $playlist_fl |\
            sed 's!.*/!!g'"
        eval "$find_comm" | sort | awk '{printf("%4d. %s\n",NR,$0)}'
    done < /tmp/fplay_list.txt
    vim -E -s $playlist_fl < ~/.scr/Ex/sort.vim
    vim -E -s $playlist_fl < ~/.scr/Ex/remove-duplicates.vim
    mpv -playlist $playlist_fl
}

function mplay {
    save_ifs="$IFS" IFS='|'
    echo -e "You searched for -> $*.\n\nYour search matched:"
    #find ~/Music -iname "*$1*" -type f -exec mplayer -profile pulse,novid,shhh {} +
    # Why not use a file listing so we can show the user, me :) what is playing
    find_iname_args=$(echo "$*" | perl -nle 'print substr(join(" ",map{qq/-iname \047*$_*\047 -o/}split/\|/),0,-3)')
    [ ! -d ~/Music ] && mkdir -p ~/Music
    paths="~/Music"
    regex="-regextype posix-extended -regex '.*(mp3|flac|wav|ogg)$' "
    find_comm="find $paths $regex $find_iname_args -type f | tee /tmp/mplay_list.txt |\
        sed 's!.*/!!g'"
    eval "$find_comm"
    mpv -playlist /tmp/mplay_list.txt &
    IFS="$save_ifs"
}

function play_dvd {
    mpv dvd://1-10
}

function wee {
    W=$(ps -ef | grep [w]eechat | awk '{print $2}')
    if [ ! -z $W ]; then
        kill -9 $W
    fi
    /usr/bin/weechat
}

function chkp {
    read -r -d '' <<-"END" USAGE
		chkp port | chkp host port
	END
    if [ $# -eq 1 ]; then
        nc -z 127.0.0.1 $1 > /dev/null 2>&1
        return $?
    elif [ $# -eq 2 ]; then
        nc -z $1 $2 > /dev/null 2>&1
        return $?
    else
        echo "$USAGE"
    fi
}

function portstat {
    read -r -d '' <<-"END" USAGE
		portstat port | portstat host port
	END
    default_host=127.0.0.1
    if [ $# -eq 1 ]; then
        if [ chkp $default_host $1 ]; then
            echo "Port $1 is up on $default_host"
        else
            echo "Port $1 is NOT up on $default_host"
        fi
    elif [ $# -eq 2 ]; then
        if [ chkp $1 $2 ]; then
            echo "Port $1 is up on $2"
        else
            echo "Port $1 is not up on $2"
        fi
    else
        echo "$USAGE"
    fi
}

function inst {
    sudo pacman -S --noconfirm "$@"
}

function psc {
    sudo pacman -Ss "$1"
}

function p {
    t=$(date +%s)
    while :
    do 
	timeout 4 ping -c 2 8.8.8.8 >& /dev/null
	[ $? -eq 0 ] && { echo "You are ON-LINE"; return; }
	echo "[$((( $(date +%s) - $t ))) secs] You are NOT ON-LINE :("
	sleep 2
    done

    # We had fun times together, but [-_-] had to grow up
    # ping 8.8.8.8
}

# Dat cool wallpaper, maybe rotate
function wallie {
    WALLPAPER=$(perl -MList::Util=shuffle -le '@walls=<~/Pictures/wallies/*>;@w=shuffle@walls;print@w[7]')
    feh --bg-fill $WALLPAPER
    # sed -n "2s/[^']*'\([^']*\)'/\1/p" ~/.fehbg

    theme=$(grep '^beautiful.init' ~/.config/awesome/rc.lua)
    theme=${theme%/*}
    theme=${theme##*/}
    awesome_=/usr/share/awesome/themes
    ln=$(grep -Hn '^ *theme.wallpaper' $awesome_/$theme/theme.lua |\
             cut -d: -f2
      )

    # Small Problem here, this depends on the files in 
    # /usr/share/awesome/themes/whatever_theme/ being writable
    # by the user running the function, which means that I need
    # to test for it!
    [ -d $awesome_/$theme -a -w $awesome_/$theme ] && {
	sed -i $ln's!\([^"]*"\).*!\1'$WALLPAPER'"!'\
            $awesome_/$theme/theme.lua 2>/dev/null;
    } || echo -e "Theme files not writable by user $(whoami)"\
	      "\nTry: sudo chgrp adm $awesome_/$theme; sudo chmod 775"\
	      $awesome_/$theme 2>&1
}
# 
# function bri {
# 	factor=$(echo "$1*46" | bc)
# 	su -c 'echo $(echo "$(cat /sys/class/backlight/intel_backlight/brightness)+$factor"|bc) > /sys/class/backlight/intel_backlight/brightness'
# }

function resm {
    kill -9 `pgrep mutt`
    tmux select-pane -thomeground:mail.0
    tmux send-keys -R -t homeground:mail.0 C-u mutt Enter
    tmux kill-pane -thomeground:mail.1
}

function swmc {
    is_local=$(ls ~/.my_* | grep 'kiboko' | wc -l)
    if [ $is_local -gt 0 ]; then
        mv ~/.my.cnf ~/.my_local.cnf
        mv ~/.my_kiboko.cnf ~/.my.cnf
    else
        mv ~/.my.cnf ~/.my_kiboko.cnf
        mv ~/.my_local.cnf ~/.my.cnf
    fi
}

function gen_tags () {
    local tags_dir=~/.tags
    [ ! -d $tags_dir ] && mkdir -p $tags_dir
    # USSD Request Parser Tags
    find ~/projects/work/ussd_request_parser -regextype posix-extended -regex '.*(php|py|sql)$' | xargs ctags -f $tags_dir/ussd_request_parser-tags
}

function tk {
    if [ $# -eq 0 ]; then
        tmux kill-session
    else
        for ses in $*; do
            tmux kill-session -t "$ses"
            [ $? -eq 0 ] && echo Terminated session: $ses
        done
    fi
}

function ta {
    if [ $# -eq 0 ]; then
        tmux ls
    else
        tmux attach -t "$1"
    fi
}

function show_progress {
    
    spin='-\|/'

    i=0
    while kill -0 $1 2>/dev/null
    do
	i=$(( (i+1) %4 ))
	printf "\r${spin:$i:1} - Chilling"
	sleep .1
    done
}

# function show_alerts {
#     nc -v 127.0.0.1 3300 > /dev/null 2>&1
#     if [ $? -ne 0 ]; then
#         echo "Sorry, mysql isn't currently forwarded."
# 

function leap_year {
    read -p "Type the year that you want to check (4 digits), followed by [ENTER]: " year
    if (( ("$year" % 400) == "0" )) || (( ("$year" % 4 == "0") && ("$year" % 100 !=
								   "0") )); then
        echo "$year is a leap year."
    else
        echo "This is not a leap year."
    fi
}



# pomodoros
function pomo {
    brief=~/.briefs/$(echo "brief-`date +'%Y-%m-%d'`.txt")
    [ -e $brief ] || touch $brief
    perms=$(stat --printf='%A' $brief)
    [ -w $brief ] || { echo $brief\'s Permissions $perms. Needs to be writable\
			    by $(whoami).; sudo chmod 664 $brief; }
    vim -E $brief
    tmux splitw -p 10 alm 20m "Current Pomodoro"
}

# Thought Interrupt - This idea has endured.
function ti {
    file=~/notes/thought-interrupt.txt
    [ -e $file ] || touch $file
    perms=$(stat --printf='%A' $file)
    [ -w $file ] || { echo $file\'s Permissions $perms. Needs to be writable\
			   by $(whoami).; sudo chmod 664 $file; }
    vim -E $file
}

# Make a work journal
function wj {
    entry=~/kazi/$(echo "Entry-`date +'%Y-%m-%d'`.txt")
    [ -e $entry ] || touch $entry
    perms=$(stat --printf='%A' $entry)
    [ -w $entry ] || { echo $entry\'s Permissions $perms. Needs to be writable\
			    by $(whoami).; sudo chmod 664 $entry; }
    vim -E $entry
}

# Make a journal entry for today
function jn {
    entry=~/Notes/journal/$(echo "Entry-`date +'%Y-%m-%d'`.txt")
    [ -e $entry ] || touch $entry
    perms=$(stat --printf='%A' $entry)
    [ -w $entry ] || { echo $entry\'s Permissions $perms. Needs to be writable\
			    by $(whoami).; sudo chmod 664 $entry; }
    vim -E $entry
}

function wp {
    # What is playing
    # what pids of mpv are running
    # check if pid is playing music (mp3)
    #     if yes get mp3 tag info if available
    #     if no file name / path is sufficient
    long=0 c=0
    [ $# -ge 1 ] && [ $1 = "-l" ] && long=1
    [ $# -ge 1 ] && [ $1 = "-p" ] && long=2
    for pid in $(ps -ef |\
                     grep -E "mpv " |\
                     grep -v "grep" |\
                     awk '{print $2}'
                )
    do
        [ $c -gt 0 ] && echo ---
        path=$(readlink /proc/$pid/fd/* |\
		   grep -Ei 'Music|Videos|Movies|rDownloads')
        file_name=$(echo "$path" |\
			# awk -F/ '{sub(/\.[^.]*$/,"",$NF);print $NF}')
			awk -F/ '{print $NF}')
        file_info=$(printf "%19s -- %s\n" "FILE" "$file_name"; echo $path |\
			perl -MMP3::Info=get_mp3tag,get_mp3info -nle '$t=get_mp3tag($_);
        $i=get_mp3info($_);print"No Tag Defined"if!defined$t;for(sort
        keys %{$t}){if(defined $t->{$_}&&!$t->{$_}=~m/^\s*$/){printf
        "%19s -- %s\n",$_,$t->{$_};}}printf"%19s -- %s\n","LENGTH",
        $i->{TIME}if defined$i->{TIME}')

        [ $long -eq 1 ]  && echo "$file_info" || { [ $long -eq 2 ] && \
						       echo $path || echo $file_name; }
        (( c++ ))
    done
}

cleanup_playlists()
{
    local arg= op= prune_list= empty_prune=
    empty_prune='\\\( *\\\) -prune'
    [[ $# -gt 0 ]] && [[ $1 =~ [[:digit:]]+ ]] && \
	arg=$1 || \
	    arg=1
    if [ $arg -eq 6 ]; then
	echo Cleaning up $(echo $(cleanup_playlists) | wc -l) playlist file\(s\).
	op=delete
    else
	op=print
    fi
    prune_list=$(ps -ef |\
		     grep mpv |\
		     grep playlist.*tmp |\
		     sed 's/.*playlist=\([^ ]*\).*/\1/' |\
		     ( path_spec=
		       while read active_playlist; do
			   path_spec="-path '$active_playlist' -o $path_spec "
		       done
		       echo $path_spec |\
			   sed 's/-o *$//'
		     ) | sed 's/.*/\\( & \\) -prune -o/'
	      )
    # echo $prune_list
    [[ $prune_list =~ $empty_prune ]] && {
	echo It appears you are not playing anything with a music playlist. >&2
	prune_list=
    }
    
    eval find ~/tmp/mpv -depth $prune_list -type f -$op
}
# I realize that leaving comments is a sort of sophisticated hubris but
# my propensity to give a duece has reduced considerably. This one's called
# just play
# TODO: Make shuffling optional, an option for the player window and 
#       verbosity.
# Ended up duplicating the function and renaming it. Sigh.
jp ()
{ 
    local exclude_file= tmp_pl=
    cleanup_playlists 6
    exclude_file=~/Music/.exclude_please
    tmp_pl=~/tmp/mpv/$(date +%Y%m%d%T | tr -d :)$(echo _$* |\
						       sed -e 's/[^[:alpha:]-]/_/g' |\
						       tr -s '_' |\
						       sed -e 's/_$//'
	  ).pl
    [ -d ~/tmp/mpv ] || mkdir -p ~/tmp/mpv
    if [[ $# -eq 0 ]]; then
        grep -v -f $exclude_file ~/Music/.song_list.txt > $tmp_pl
            mpv --playlist=$tmp_pl \
                --shuffle > /dev/null 2>&1 &
    elif [[ $# -eq 1 ]]; then
        grep -v -f $exclude_file \
             <(grep -i "$1" ~/Music/.song_list.txt) |\
            sort > $tmp_pl
            mpv --playlist=$tmp_pl \
            --shuffle > /dev/null 2>&1 &
    else
        # Could this be prettier? I don't know, your suggestions are welcome.
        grep_str="grep -v -f $exclude_file <(grep -i -e '$1'"
        for a in $(seq 2 $#); do grep_str="$grep_str -e '${!a}'"; done
        grep_str="$grep_str ~/Music/.song_list.txt) | sort > $tmp_pl"
        eval $grep_str
        mpv --playlist=$tmp_pl \
            --shuffle > /dev/null 2>&1 &
    fi
    # cd -
}

jpo ()
{ 
    local exclude_file= tmp_pl=
    cleanup_playlists 6
    exclude_file=~/Music/.exclude_please
    tmp_pl=~/tmp/mpv/$(date +%Y%m%d%T | tr -d :)$(echo _$* |\
						       sed -e 's/[^[:alpha:]-]/_/g' |\
						       tr -s '_'|\
						       sed -e 's/_$//'
	  ).pl
    [ -d ~/tmp/mpv ] || mkdir -p ~/tmp/mpv
    if [[ $# -eq 0 ]]; then
	grep -v -f $exclude_file ~/Music/.song_list.txt > $tmp_pl
        mpv --playlist=$tmp_pl \
            > /dev/null 2>&1 &
    elif [[ $# -eq 1 ]]; then
	grep -v -f $exclude_file \
	     <(grep -i "$1" ~/Music/.song_list.txt) |\
	    sort > $tmp_pl
        mpv --playlist=$tmp_pl \
	    > /dev/null 2>&1 &
    else
        # Could this be prettier? I don't know, your suggestions are welcome.
        grep_str="grep -v -f $exclude_file <(grep -i -e '$1'"
        for a in $(seq 2 $#); do grep_str="$grep_str -e '${!a}'"; done
        grep_str="$grep_str ~/Music/.song_list.txt) | sort > $tmp_pl"
	eval $grep_str
        mpv --playlist=$tmp_pl \
            > /dev/null 2>&1 &
    fi
}

# This is refresh and play.
rnp () 
{ 
    find ~/Music \( -type f \( -name '*m4a' -o -name '*3' -o -name '*flac' -o \
	 -name '*mp4' -o -name '*mkv' -o -name '*webm' \) \) > ~/Music/.song_list.txt;
    # cd ~/Music;
    # mpv -profile pulse,shh --playlist=.song_list.txt --shuffle
    jp "$@"
}

rpl () 
{ 
    find ~/Music \( -type f \( -name '*m4a' -o -name '*3' -o -name '*flac' -o \
	 -name '*mp4' -o -name '*mkv' \) \) > ~/Music/.song_list.txt;
    # cd ~/Music;
    # mpv -profile pulse,shh --playlist=.song_list.txt --shuffle
}

vp () 
{ 
    cd ~/Videos
    if [[ $# -eq 0 ]]; then
        mpv --playlist=.vid_list.txt --shuffle \
            >& /dev/null &
    elif [[ $# -eq 1 ]]; then
        mpv --playlist=<( grep -i "$1" .vid_list.txt ) \
            --shuffle >& /dev/null &
    else
        # Could this be prettier? I don't know, your suggestions are welcome.
        grep_str="grep -i -e '$1'"
        for a in $(seq 2 $#); do grep_str="$grep_str -e '${!a}'"; done
        grep_str="$grep_str .vid_list.txt"
        mpv --playlist=<( eval "$grep_str" ) \
            --shuffle >& /dev/null &
    fi
    cd -
}

# The u-variants were meant for the mounted hdd.
uvp () 
{ 
    cd ~/Videos
    if [[ $# -eq 0 ]]; then
        mpv --playlist=hdd-list.txt --shuffle
    elif [[ $# -eq 1 ]]; then
        mpv --playlist=<( grep -i "$1" hdd-list.txt )\
            --shuffle
    else
        # Could this be prettier? I don't know, your suggestions are welcome.
        grep_str="grep -i -e '$1'"
        for a in $(seq 2 $#); do grep_str="$grep_str -e '${!a}'"; done
        grep_str="$grep_str hdd-list.txt"
        mpv --playlist=<( eval "$grep_str" ) --shuffle
    fi
    cd -
}

urvp () 
{ 
    find '/mnt/usb/MOVIES LIB' '/mnt/usb/HOME VIDEOS/' '/mnt/usb/MOVIES MIKE' \
        -type f -print0 2> /dev/null | xargs -0 file 2> \
        /dev/null | perl -ne 'print if
        !/text|audio|composite|srt|mp3|image|sub:|wma:|meta:|archive|
        sample|epub:|jpg:|txt:/i' \
        | sed 's/\(\.[^:]*\):.*/\1/' > ~/Videos/hdd-list.txt;
    uvp "$@"
    # mpv --playlist=.vid_list.txt;
}

rfv () 
{ 
    find ~/rDownloads/ ~/Music/ ~/Videos/ -type f -print0 2> /dev/null | xargs -0 file 2> /dev/null | perl -ne 'print if !/text|audio|composite|srt|mp3|image|sub:|wma:|meta:|archive|sample|epub:|jpg:|txt:|bmp:|#:|swp:/i' | sed 's/\(\.[^:]*\):.*/\1/' > ~/Videos/.vid_list.txt;
    # mpv --playlist=.vid_list.txt;
}

vtog()
{
    local mpv_conf_dir= on= off= \
	  vfile=
    mpv_conf='.config/mpv'
    on=scripts off=.script_off
    vfile=$(find $HOME/$mpv_conf -name 'visualizations.lua')
    grep -q $off <<< $vfile
    if [ $? -eq 0 ]
    then
	mv $vfile $HOME/$mpv_conf/$on
    else
	mv $vfile $HOME/$mpv_conf/$off
    fi    
}

pl()
{
    local files=0 list=1 pids=0 songs=0
    while getopts :fls OPT; do
        case $OPT in
            f)
                files=1
                list=0
                ;;
            l)
                list=1
                ;;
            s)
                songs=1
                list=0
                ;;
            *)
                echo "usage: ${FUNCNAME[0]} [-fls} [--]"
                return
                ;;
        esac
    done
    shift $(( OPTIND - 1 ))
    OPTIND=1
    [ $files -eq 1 ] && {
        ps -ef |\
            grep [m]pv |\
            awk -F' --' '{print $2}' |\
            sed 's/[^=]*=//'
    }
    
    [ $list -eq 1 ] && {
        printf "%-16s\t%-s\n" "Time Played" "Search Key"
        printf "%-16s\t%-s\n" "------------" "----------"
        ps -ef |\
            grep [m]pv |\
            awk -F' --' '{print $2}' |\
            sed 's/[^=]*=//' |\
            awk -F'/' '{print $NF}' |\
            sed 's/\(....\)\(..\)\(..\)\(..\)\(..\)...\([^.]*\).*/\1-\2-\3 \4:\5\t\6/'
    }

    # [ $songs -eq 1 ]

}

ts ()
{
    [ $# -gt 1 ] || { echo You need to provide the target pane\
        and keys you are looking to send yow.; return 2; }
    target_pane=$1
    shift
    tmux send -l -t $target_pane "$*"
}

dln ()
{
    # Quick and dirty function to start a download session
    # in tmux
    tmux new -s dln -d
    tmux split-window -t dln:1
    tmux send-keys -t dln:1.0 rtorrent Enter
    tmux send-keys -t dln:1.1 lynx Space 1337x.to Enter
    tmux select-pane -t dln:1.1
    tmux set-option -g status off
    tmux set-window-option -g pane-border-style fg=yellow
    tmux set-window-option -g pane-active-border-style fg=red
    # tmux send-keys -t dln:1.0 9 Enter
    tmux attach -t dln
}

progress () 
{ 
    [ $# -ge 1 ] || { echo "Usage: ${FUNCNAME[0]} PID [ MESSAGE ]"; \
        return; }
    pid=$1;
    spin='-\|/';
    while kill -0 $pid 2> /dev/null; do
        i=$(( (i+1) % 4));
        printf "\r$2 -- ${spin:$i:1}";
        sleep .3;
    done
}

ils() 
{ 
    # prj=$(grep project <(gcloud config list 2>/dev/null) | cut -d' ' -f3)
    prj=$(cat ${HOME}/.text/.current_project.txt)
    instance_list=${HOME}/.text/.${prj}_instance_list.txt
    [ $# -eq 0 ] && cat $instance_list || \
        eval grep $(for o in $@; do printf -- "-e $o "; done) -i $instance_list
}

rgc () 
{ 
    [ -d $HOME/.text ] || mkdir $HOME/.text
    grep project <(gcloud config list 2>/dev/null) | cut -d' ' -f3 \
        > $HOME/.text/.current_project.txt
    gcloud compute instances list > \
        ${HOME}/.text/.$(cat ${HOME}/.text/.current_project.txt)_instance_list.txt
}

gsh () 
{ 
    timeout 4 ping -c 2 8.8.8.8 >& /dev/null
    online=$?
    args=$#; srch=$1; shift
    instance=$(ils | grep -i "$srch" | awk '{print $1}' | head -1)
    [ $args -eq 1 ] && {
        echo -e Attempting to log in to "$YELLOW$instance$NC"
        [ $online -eq 0 ] && gcloud compute ssh $instance -- "$@" || echo -e \
        \\nYou are ${YELLOW}not$NC online :\(
    } || [ $online -eq 0 ] && {
            echo -e Attempting to log in to "$YELLOW$instance$NC" and run \
            commands.
            gcloud compute ssh $instance -- "$@";
            # The last command in a list is the source of the list's
            # return value. The trap grabs Ctrl-c and supplies a 0
            # as the list's return val. 2 Birds.
            trap return INT
    } || echo -e \\nYou are ${YELLOW}not$NC online :\( 
}

pures ()
{
    # Wow, just wow!
    # I have looked for this for a long time, thank you user: mikewhatever
    # https://askubuntu.com/questions/230888/is-there-another-way-to
    #   -restart-the-sound-system-if-pulseaudio-alsa-dont-work?newreg=
    #   80e91c8d99aa487682cbbb84975433b0ubuntu forums.
    # Progress really is a game of inches huh!
    pulseaudio -k && sudo alsa force-reload 2>/dev/null
}

vl()
{
    unzip -c $(ls -drt ~/.weechat/xfer/* | tail -1) | less -
}

lf()
{
    local args= path= lines=
    # path/to/file 20 path/to/file -2 -5 using regex, naughty naughty
    [ $# -gt 0 ] && \
        args=$(perl -nle '($p)=/([~\*\/.a-zA-Z_]+)/;($l)=/([-\d]+)\b/;
        $p=~s!/$!!;printf"%s^^%s\n",(length($p)==0)?"NO_NAME":$p,
        (length($l)==0)?"NO_LINES":$l' <<< "$@") || args='*'
        #echo $args
    path=$(awk -F'\\^\\^' '{print $1}' <<< $args)
    lines=$(awk -F'\\^\\^' '{print $2}' <<< $args | tr -d '-')
    [[ $lines =~ ^NO_LINES$ ]] && lines=5
    [[ $path =~ ^NO_NAME$ ]] && ls -drtA $(pwd)/* | tail -$lines || \
        ls -drtA $path/* | tail -$lines
}

llf()
{
    local args= path= lines=
    # path/to/file 20 path/to/file -2 -5 using regex, naughty naughty
    [ $# -gt 0 ] && \
        args=$(perl -nle '($p)=/([~\*\/.a-zA-Z_]+)/;($l)=/([-\d]+)\b/;
        $p=~s!/$!!;printf"%s^^%s\n",(length($p)==0)?"NO_NAME":$p,
        (length($l)==0)?"NO_LINES":$l' <<< "$@") || args='*'
        #echo $args
    path=$(awk -F'\\^\\^' '{print $1}' <<< $args)
    lines=$(awk -F'\\^\\^' '{print $2}' <<< $args | tr -d '-')
    [[ $lines =~ ^NO_LINES$ ]] && lines=5
    [[ $path =~ ^NO_NAME$ ]] && ls -ldrtA $(pwd)/* | tail -$lines || \
        ls -ldrtA $path/* | tail -$lines
}
define ()
{
    local wrd= dict=
    dict='https://www.merriam-webster.com/dictionary/'
    [ $# -ne 1 ] && {
        echo Usage: define word 1>&2
        return 2
    } || {
        wrd=$1
    }
    lynx \
        -nonumbers \
        -dump $dict$wrd |\
        sed -n '/^Definition/,/^Learn More about /{
                                      /Keep scrolling for more/d
                                      /See More.*\.svg/d
                                      /^Learn More about/!p
                }' |\
    less
}
