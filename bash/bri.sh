#!/bin/bash

multiple=$( echo $1 | sed 's/^\+*//' )

function bri {
	bval=$(cat /sys/class/backlight/intel_backlight/brightness)
	factor=$(echo "$1*46" | bc)
	new_brightness_val=`expr $bval + $factor`
	if [ $new_brightness_val -lt 5 ]; then
		echo "Can't reduce brightness below lower limit of 5. Try a smaller factor. [$new_brightness_val]"
		exit 3
	elif [ $new_brightness_val -gt 925 ]; then
		echo "Can't increase brightness beyond upper limit of 925. Try a smaller factor. [$new_brightness_val]"
		exit 4
	fi
	eval sudo echo $new_brightness_val > /sys/class/backlight/intel_backlight/brightness
}

# Do the thing.
[ $# -gt 0 ] && bri $multiple
[ $# -gt 0 ] || echo -e "You need to provide args :(\n"
