#!/bin/bash

function leet {
    echo "$1" | tr 'abcdefghijklmnopqrstuvwxyz' '4bcd3fgh1jklmn0pqr57uvwxy2'
}
