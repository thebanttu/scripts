#!/bin/bash


function trot {
    echo "$@" | tr 'A-Ma-mN-Zn-z' 'N-Zn-zA-Ma-m'
}


function trat {
    echo "$@" | tr 'N-Zn-zA-Ma-m' 'A-Ma-mN-Zn-z'
}
