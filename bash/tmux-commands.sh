#!/bin/bash

function lsp {
    a=$(tmux list-panes -a -F '#{pane_id} #P #T #S #{window_name} #{pane_current_command} #{pane_current_path} #{pane_active} #{pane_in_mode} #{pane_pid} #{}')
    echo "$a"
}

function lsw {
    a=$(tmux list-windows -a -F '#{window_id} - Session:#S #{window_name}')
    echo "$a"
}

function llk {
    tmux source-file ~/.mktmx.conf
}

function llwk {
    tmux source-file ~/.mkwtmx.conf
}

function gwstat {
    # Average Throughput
    avg_throughput=$(bgstat | sed -n -e '/^SMS:.*outbound (/p' | awk '{gsub(/\(|\)|,.*/,"",$6);print $6}')
    # Store size
    store_size=$(bgstat | sed -n '/.*store size \([0-9][0-9]*\)$/s//\1/p')
    gw_status="St.S: $store_size | AvThr: $avg_throughput"
    echo "$gw_status"
}

function bulkstat {
    # Gateway Stats
    gw_stats=$(gwstat)
    # Priority Queue
    q_priority='QPr: #(echo "select count(*) from kannel.bulk_send_sms" | mysql\
        -P3300 -ushadrack -pk1k0h0z1 -h127.0.0.1 | sed 1d)'
    # Non Priority Queue
    q_non_priority='QNPr: #(echo "select count(*) from kannel.bulk_send_sms_np" | mysql\
        -P3300 -ushadrack -pk1k0h0z1 -h127.0.0.1 | sed 1d)'
    # Total sent messages from alerts table
    total_sent='TS: #(echo "select sum(total_sent) from
        smsleo_v1_transaction.alerts where date(send_time) = curdate()" | mysql\
        -P3300 -ushadrack -pk1k0h0z1 -h127.0.0.1 | sed 1d)'
    date_time='%a %h-%d %H:%M'
    # Finally Everything together
    r_status="$gw_stats | $q_priority | $q_non_priority | $total_sent | $date_time"
    tmux set-option -g status-right "$r_status"
}

function chk_start_session {
    if [ $# -eq 0 ]; then
        echo "Please provide a session namme."
        return 2
    fi
    tmux has-session -t $1 &> /dev/null
    if [ $? -eq 0 ]; then
        echo "Session is already started"
        return 7
    else
        echo "Starting new session $1"
        tmux new-session -s "$1" -d
        return 7
    fi
}

function g1ws {
    chk_start_session work
    tmux rename-window -t work:1 gw1
    tmux send-keys -R -t work:gw1.0 ssh Space gw1 Enter
    retval=$?
    [ $retval -eq 0 ] && tmux send-keys -R -t work:gw1.0 su Enter
    retval=$?
    [ $retval -eq 0 ] && tmux send-keys -R -t work:gw1.0 n3wP@ss4r00t Enter
}

function plpath {
    wp -p | grep -v -- ---
}

function trw {
    local save_ifs= pl= playing=
    pl=~/Music/Playlists/.treesworthy.txt
    playing=$(plpath)
    if [ $(wc -l <<< "$playing") -gt 1 ]
    then
        save_ifs=$IFS; IFS=$'\n'
        select path in $(plpath) "Quit."
        do
            [[ $path =~ uit\.$ ]] && break
            grep -Fxq $path $pl
            [ $? -eq 0 ] || echo $path >> $pl
        done
        IFS=$save_ifs
    else
        echo $playing >> $pl
    fi
}

function nw {
    # Retired on 
    # echo $(wp -p | grep -v -- ---) >> ~/Music/Playlists/.noteworthy.txt
    
    # You cannot fight progress.
    # Bugfix for using mpv as primary player
    # echo $(echo $(tmux capturep -J -p -t media:Music.0 |\
    #        grep 'Playing:' | tac | head -1 |\
    #        awk 'BEGIN{FS=": "}{print $NF}' | uniq) >>\
    #        ~/Music/Playlists/.noteworthy.txt
    # ) 
    # tmux capture-pane -p -t %9 -S - -E - | grep ^Pla | \
    #     sed "s/^Playing \(.*\).$/'\1'/" | \
    #     tail -1 >>  ~/Music/Playlists/.noteworthy.txt; \
    #     vim -E -s ~/Music/Playlists/.noteworthy.txt < \
    #     ~/scripts/Ex/remove-duplicates.vim
    local save_ifs= pl= playing=
    pl=~/Music/Playlists/.noteworthy.txt
    playing=$(plpath)
    if [ $(wc -l <<< "$playing") -gt 1 ]
    then
        save_ifs=$IFS; IFS=$'\n'
        select path in $(plpath) "Quit."
        do
            [[ $path =~ uit\.$ ]] && break
            grep -Fxq $path $pl
            [ $? -eq 0 ] || echo $path >> $pl
        done
        IFS=$save_ifs
    else
        echo $playing >> $pl
    fi
}

function savedp {
    tmux capturep -S - -E - -J -p -t media:Music.0 |\
        grep 'Playing:' | tac | awk\
        'BEGIN{FS=": "}{print $NF}' | sort | uniq >>\
        ~/Music/Playlists/.dlyplst/$(date +%Y-%m-%d)_pl

}
