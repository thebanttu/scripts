#!/bin/bash

function lsp {
    a=$(tmux list-panes -a -F '#{pane_id} - #S:#{window_name} [comm:#{pane_current_command}] [path:#{pane_current_path}]')
    echo "$a"
}

function lsw {
    a=$(tmux list-windows -a -F '#{window_id} - Session:#S #{window_name}')
    echo "$a"
}
