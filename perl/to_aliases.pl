#!/usr/bin/perl -n

use strict;
use warnings;

parse($_) if /To:/;


sub parse {
    my @addresses = split/,/;
    for (@addresses) {
        $_ =~ s/(?:To: |^\s+|\s+$)//;
    }
    print "This Email -> " . $_ . "\n" for @addresses;
}
