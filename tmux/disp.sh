#!/bin/bash
[ ! -d ~/Music ] && mkdir ~Music/{Hiphop,Neosoul,Rock,Reggea,Sounds,Dance}
play='find ~/Music \( -type f \( -name "*3" -o -name "*flac" \) \) -exec mpv -profile pulse -shuffle {} +'
tmux bind-key ^ send-keys -l -R -t homeground:music "$play"
