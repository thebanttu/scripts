#!/bin/bash

# Start Screensaver
xscreensaver -no-splash >& /dev/null &

# Start Browsers (graphical)
# opera-beta >& /dev/null &
# firefox >& /dev/null &

# Include tmux-functions
source ~/scripts/tmux/tmux-functions.sh

# Include tmux-variables
. /home/bantu/scripts/tmux/vars.sh


# Create homeground session
tmux new -c ~ -A -s 'homeground' -d

# Create some windows and panes
tmux rename-window -t homeground:1 'mainspace'
tmux new-window -c ~ -n 'mail' -d
# I do not think I really need to have a browser window
# tmux new-window -c ~ -n 'web' -d
tmux new-window -c ~ -n 'downloads' -d
tmux split-window -c ~ -v -p 30 -t homeground:downloads -d
tmux new-window -c ~ -n 'chat' -d
# tmux new-window -n 'gubhtugf' -c ~/Notes -d
# tmux new-window -c ~/Documents/Books -n 'reference' -d
tmux new-window -c ~ -n 'irc' -d


# Start some things

# Start Mail Client.
tmux send-keys -R -t homeground:mail mutt Enter

# Fungua browser ... ya text
# tmux send-keys -l -R -t homeground:web "lynx google.com"

# Downloads window
tmux send-keys -R -t homeground:downloads.0 rtorrent Enter
tmux send-keys -R -t homeground:downloads.1 lynx Space thepiratebay.org Enter

# Chat
tmux send-keys -R -t homeground:chat finch Enter

# Notes
# today string for journaling -> journal/Entry-2017-12-16.txt
today_string=$(echo "Entry-`date +'%Y-%m-%d'`.txt")
# tmux send-keys -R -t homeground:gubhtugf vim Space -S Space ~/_vim/.vimrc Space -c"'"\$"'" Space "~/Notes/journal/$today_string" Space ~/Notes/thoughts.txt Space ~/Notes/work-sheet.txt Space ~/Notes/to-do.txt Space ~/Notes/thought-interrupt.txt Space Enter

# Books
# tmux send-keys -R -t homeground:reference ls Enter pdft Space 

# IRC
tmux send-keys -R -t homeground:irc wee Enter
tmux rename-window -t homeground:5 'irc'
# tmux send-keys -R -t homeground:yrneavat-rznpf emacs Space -nw Enter

# Homeground Bindings
tmux source-file ~/scripts/tmux/homeground-keys.conf

# Attach to homeground
#                      or not
# tmux at -t 'homeground'

# These currently only work for my home machine
# -------------------------------------------------
# tmux new -c ~ -A -s 'media' -d
# tmux new-window -t 'media' -n 'music' -c ~/Music -d
# -------------------------------------------------

tmux new -c ~ -A -s 'media' -d
tmux new-window -c ~ -t 'media' -n 'Music' -d

#play='find -path ./Neosoul -prune  -o \( -type f \( -name '*3' -o -name '*flac' \) \) -exec mplayer -ao alsa -shuffle {} +'
# play='find ~/Music \( -type f \( -name '*3' -o -name '*flac' \) \) >'
# play+=' ~/Music/.song_list.txt;cd ~/Music; mpv -profile pulse'
# play+=' --playlist=.song_list.txt --shuffle'

# Media Bindings
tmux source-file ~/scripts/tmux/media-keys-wk.conf
# tmux source-file ~/scripts/tmux/brightness-keys.conf

# Start playing music without asking questions. Donno, this feels a bit
# unrefined.
tmux send-keys -l -R -t media:Music jp

# Binding for above
tmux bind-key ^ send-keys -l -R -t media:Music "$play"



# Set some options

# Monitor activity on the following windows.
# tmux set-window-option -thomeground:chat monitor-activity on
tmux set-window-option -thomeground:mail monitor-activity on
tmux set-window-option -thomeground:irc monitor-activity on

# Finally bring up the choose session menu, think send_prefix!!
# tmux send-prefix -t homeground:mainspace; tmux send-keys -R -t homeground:mainspace C-s$
