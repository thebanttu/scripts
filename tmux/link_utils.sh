#!/bin/sh

tmux link-window -d -s homeground:mail  -t work:
tmux link-window -d -s homeground:gubhtugf -t work:
tmux link-window -d -s homeground:web -t work:
tmux link-window -d -s homeground:mainspace -t work:

