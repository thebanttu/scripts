#!/bin/bash

function link_utilities {
    tmux link-window -d -s homeground:mail  -t work:
    tmux link-window -d -s homeground:tasks -t work:
    tmux link-window -d -s homeground:web -t work:
}

# Create session
function tns {
    tmux new -c ~ -A -s "$1" -d
}

# Create window
function tnw {
    case $# in
        0) tmux new-window -c ~ ;;
        1) tmux new-window -c ~ -n "$1" ;;
        2) tmux new-window -c ~ -t "$1" -n "$2" ;;
        *) echo "Not sure what you want me to do with $#"\
            "arguments"
           exit 2
           ;;

    esac
}

# Rename Window
function trw {
    [ $# -eq 2 ] || { echo "Invalid number of args provided"\
        ". Expected 2 args got $#."; exit 2; }
    tmux rename-window -t "$1" "$2"
}
