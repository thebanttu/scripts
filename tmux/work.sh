#!/bin/bash

# Include tmux-functions
source /home/bantu/scripts/tmux/tmux-functions.sh

# Create work session
tmux new -A -s 'work' -d

# Create some windows and panes
tmux rename-window -t work:0 'shell'
tmux new-window -n 'kingslanding' -d
tmux new-window -n 'drangonstone' -d
tmux new-window -n 'castleblack' -d
tmux new-window -n 'pyke'
tmux new-window -n 'winterfell' -d
tmux new-window -n 'highgarden' -d

# Link Utilities
link_utilities
# Run some commands

# Attach to work. 
#                   Or Not..

# tmux at -t 'work'
